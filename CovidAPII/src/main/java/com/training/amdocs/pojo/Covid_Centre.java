package com.training.amdocs.pojo;

public class Covid_Centre {

	private String id;
	private String name;
	private String address;
	private String pin;

	public Covid_Centre(String id, String name, String address, String pin) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.pin = pin;
	}

	public Covid_Centre() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	@Override
	public String toString() {
		return "Covid_Centre [id=" + id + ", name=" + name + ", address=" + address + ", pin=" + pin + "]";
	}

}
