package com.training.amdocs.services;

import org.springframework.stereotype.Service;

import com.training.amdocs.pojo.Covid_Centre;

import java.util.*;

@Service
public class ReviewCareService {
	List<Covid_Centre> centres;

	public ReviewCareService() {
		centres = new ArrayList<Covid_Centre>();

		centres.add(new Covid_Centre("1", "ABC", "Kolhapur", "12345"));
		centres.add(new Covid_Centre("2", "SDF", "Pune", "234567"));
		centres.add(new Covid_Centre("3", "DFG", "Delhi", "345678"));
		centres.add(new Covid_Centre("4", "FGH", "Mumbai", "456789"));
	}

	public List<Covid_Centre> getAllCentres() {

		return centres;
	}

	public Covid_Centre getCentre(String pin) {

		Covid_Centre centre1 = null;

		for (Covid_Centre centre : centres) {
			if (centre.getPin().equalsIgnoreCase(pin)) {
				centre1 = centre;
				break;
			}
		}

		return centre1;

	}

	public String addCentre(String id, String name, String address, String pin) {
		Covid_Centre centre = new Covid_Centre(id, name, address, pin);
		centres.add(centre);

		return "New Centre Inserted";

	}
}
