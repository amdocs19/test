package com.training.amdocs.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.training.amdocs.pojo.Covid_Centre;
import com.training.amdocs.services.ReviewCareService;

import java.util.*;

@RestController
public class CentreController {
	
	@Autowired
	ReviewCareService reviewCareService ;
	
	@GetMapping("/allCentres")
	public List <Covid_Centre> getAllcentres(){
		return reviewCareService.getAllCentres();
	}
	
	@GetMapping("/allCentres/{pin}")
	public Covid_Centre getCentre(@PathVariable String pin) {
		return reviewCareService.getCentre(pin);
	}
	
	@PostMapping("/covidCentre")
	public String addCovidCentre(@RequestBody Covid_Centre centre) {
		return reviewCareService.addCentre(centre.getId(), centre.getName(), centre.getAddress(), centre.getPin());
	}

}
