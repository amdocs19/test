package com.training.amdocs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CovidApiiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CovidApiiApplication.class, args);
	}

}
